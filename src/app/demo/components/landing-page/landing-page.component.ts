import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  items: any[] = [

    {
      bannerText: "KARIBA REDD+ - ZIMBABWE",
      title: "Reduced Emissions from Deforestation and Degradation",
      offset: "957,8421bs. offset",
      summary: "Protects forests and wildlife on the southern shores of Lake Kariba in Zimbabwe, forming a giant biodiversity corridor.",
      rate: "7",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "ARCATA SUNNYBRAE TRACT - USA",
      title: "Improved Forest Management",
      offset: "710,093 lbs. offset",
      summary: "Preservation of large tracts of coastal Redwood and Douglas fir that were slated for harvesting by the timber company.",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "WOLF CREEK LNDFALL - USA",
      title: "Landfill Methane Gas Capture",
      offset: "2,005,214 lbs. offset",
      summary: "Landfill methane gas - to - energy facility that provides significant environmental benefits to the surrounding area.",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "TERRAPASS MIXED PORTFOLIO - MIXED",
      title: "Mixed",
      offset: "241, 967 lbs.offset",
      summary: "Greenhouse gas reductions made at a variety of projects, including: reforestation, methane capture, and renewable energy. ",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "GASTON COUNTY LNADFILL - USA",
      title: "Landfill Methane Gas Capture",
      offset: "721,663 lbs. offset",
      summary: "Voluntary collection and destruction of landfill methane in two open flares and three internal combustion engine generators.",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "MYAMYN CONSERVATION - AUSTRALIA",
      title: "Reforestation & Hyrdopower",
      offset: "995, 908 lbs.offset",
      summary: "Supporting biodiversity and reforestration in Victoria, Australia; along with carbon reduction from a hyrdopower plant in Vietnam.",
      imageLink: "/assets/image1.jpg",
      rate: 7.50
    },
    {
      bannerText: "NATURAL PACT LAND ART - COSTA RICA",
      title: "Reforestation through Land Art",
      offset: "3271bs.offset",
      summary: "Unique \"land art\" reforestation and conservation of forests that extend an important biological corridor for many endangered species.",
      imageLink: "/assets/image1.jpg"
    },
    {
      bannerText: "NATURE LAB URBAN FORESTRY - CANADA",
      title: "Urban Reforestation",
      offset: "13, 033 lbs.offset",
      summary: "Rehabilitating degraded urban and sub - urban areas through reforestation, adding significant greenery and restoring biodiversity.",
      imageLink: "/assets/image1.jpg",
      rate: 22.50
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

}

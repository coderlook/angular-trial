import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { DemoRoutingModule } from './demo-routing.module';
import { SingleItemComponent } from './components/single-item/single-item.component';


@NgModule({
  declarations: [LandingPageComponent, SingleItemComponent],
  imports: [
    CommonModule,
    DemoRoutingModule
  ]
})
export class DemoModule { }
